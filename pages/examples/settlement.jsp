
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Settle acount
                        <small></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="#">Travel Agent</a></li>
                        <li class="active">Settle acount</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">

                <!-- START CUSTOM TABS -->
                <div class="row">
                    <div class="col-md-12">
                    	<div class="nav-tabs-custom">
				<div class="tab-content">
                    			<div class="tab-pane active" id="tab_1">
                    				<div class="row"  style="margin-left:0px">
										<div class="col-xs-5"  style="background-color:#FFF7D4;">
											<div class="your_summary">
											    <h3>Settle account</h3>
											    <ul>
											    	<li><a href="#">Order item No.  90891</a></li>
											    </ul>
		                                    	<div style="text-align:right;padding : 5px;margin : 5px;">
		                                    		<small class="badge">Add any more ?</small>
													
		                                    	</div>
												<div class="line"/>
										      	<div class="prices_detail">
										                <div class="detail_list">
										                    <h4>Amount Total</h4>
										                    <table style="width:100%">
										                        <tbody>                            
										                                <tr id="adtPriceInSummary" style="display: ;">
										                                    <th>
										                                        Amount Total
										                                    </th>
										                                    <td style="text-align:right">
																				<b>2,180</b>
																				&nbsp;HKD
																			</td>
										                                </tr> 
										                                <tr id="chdPriceInSummary" style="display: ;">
										                                    <th id="chdPriceTitleInSummary">
										                                        Settlement service fee
										                                    </th>
										                                    <td style="text-align:right">
																				<input type="text" placeholder="unit price">
																				&nbsp;HKD
										                                   </td>
										                                </tr>  
										                        </tbody>
										                    </table>
										                </div>
										            </div>
													<div class="line"/>
										            <dl class="total">
										                <dt><h4>Total</h4></dt>
										                <dd style="text-align:right">                
										                    
										                    HKD&nbsp;2,180
										                </dd>
										            </dl>
										        </div>	
									        
										        
										</div>	      			
										
										<div class="col-xs-7">
											
											
<div class="pageFormContent">
		<table width="99%" id="tab_paymentMethod">
		  <tbody><tr>
			<td width="5%" align="center" valign="middle" height="80px"><input name="paymentMethod" type="radio" svalue="Cash" value="200300"></td>
			<td width="10%" valign="middle"><img src="/apple_travel/styles/images/payment/Cash.png"></td>
			<td width="14%" valign="middle">Cash</td>
			
			<td width="5%" align="center" valign="middle"><input name="paymentMethod" type="radio" svalue="Credit Card" value="200301"></td>
			<td width="10%" valign="middle"><img src="/apple_travel/styles/images/payment/credit_card.png"></td>
			<td width="14%" valign="middle">Credit<br>Card</td>
			
			<td width="5%" align="center" valign="middle"><input name="paymentMethod" type="radio" svalue="Cheque" value="200302"></td>
			<td width="10%" valign="middle"><img src="/apple_travel/styles/images/payment/cheque.png"></td>
			<td width="14%" valign="middle">Cheque</td>
		  </tr>
		  <tr>
			<td align="center" valign="middle"><input name="paymentMethod" type="radio" svalue="EPS" value="200304"></td>
			<td valign="middle"><img src="/apple_travel/styles/images/payment/EPS.png"></td>
			<td valign="middle">EPS</td>
			
			<td align="center" valign="middle"><input name="paymentMethod" type="radio" svalue="Transfer" value="200303"></td>
			<td valign="middle" class=""><img src="/apple_travel/styles/images/payment/Transfer.png" width="86" height="40"></td>
			<td valign="middle">Transfer</td>
			
			<td align="center" valign="middle">&nbsp;</td>
			<td valign="middle">&nbsp;</td>
			<td valign="middle">&nbsp;</td>
		  </tr>
		</tbody></table>
		<div class="line" />
		<table width="100%" height="90" border="0" cellspacing="0" cellpadding="0" id="tab_cash" style="display: table;">
		</table>
		<table width="100%" height="90" border="0" cellspacing="0" cellpadding="0" id="tab_creditCardType" style="display: none;">
		  <tbody><tr>
			<td align="center"><input name="creditCardType" type="radio" value="200701" svalue="VISA"></td>
			<td><img src="/apple_travel/styles/images/payment/VISA.png" width="64" height="40"></td>
			<td><input name="creditCardType" type="radio" value="200700" svalue="Master Card"></td>
			<td><img src="/apple_travel/styles/images/payment/MasterCard.png" width="65" height="40"></td>
			<td><input name="creditCardType" type="radio" value="200702" svalue="AE"></td>
			<td><img src="/apple_travel/styles/images/payment/AE.png" width="63" height="40"></td>
			<td><input name="creditCardType" type="radio" value="200703" svalue="JCB"></td>
			<td><img src="/apple_travel/styles/images/payment/JCB.png" width="59" height="40"></td>
			<td><input name="creditCardType" type="radio" value="200704" svalue="CUP"></td>
			<td><img src="/apple_travel/styles/images/payment/CUP.png" width="59" height="40"></td>
		  </tr>
		  <tr>
		  	<td>Card No.:</td>
			<td height="50" colspan="10"><input type="text" id="cardNum" class="textInput">
			  <label>
			  <input type="checkbox" name="checkbox" id="chargeForm">
		    Charge Form</label></td>
		  </tr>
		</tbody></table>
		<table width="100%" height="90" border="0" cellspacing="0" cellpadding="0" id="tab_cheque" style="display: none;">
			<tbody><tr>
				<td width="100">Cheque No.:</td>
				<td height="50" colspan="10" align="left">
					<input id="chequeNo" type="text" style="align:center;" class="textInput">
				</td>
				<td>Bank:</td>
				<td>
					<input type="hidden" id="bank" name="payment_method_details.bank.id">
					<input type="text" id="bank_show" class="textInput" name="payment_method_details.hi_bank.bank_short_name" size="12" autocomplete="off" lookupgroup="payment_method_details" lookupname="bank" suggestclass="org.jxf.appletravel.baseinfo.model.Bank_info" searchfields="bank_short_name">
					<a class="btnLook" href="bank_infoLookup.action?lookup=1" lookupgroup="payment_method_details" lookupname="bank" title=""></a>
				</td>
			</tr>
		</tbody></table>
		<table width="100%" height="90" border="0" cellspacing="0" cellpadding="0" id="tab_transfer" style="display: none;">
			<tbody><tr>
				<td width="20%">Transfer No.:</td>
				<td height="50" colspan="10">
					<input id="transferNo" value="" type="text" style="align:center;" class="textInput">
				</td>
			</tr>
		</tbody></table>
		<table width="100%" height="90" border="0" cellspacing="0" cellpadding="0" id="tab_EPS" style="display: none;">
			<tbody><tr>
				<td width="20%">EPS Payer Name:</td>
				<td height="50" colspan="10">
					<input id="EPSPayerName" value="" type="text" style="align:center;" class="textInput">
				</td>
			</tr>
		</tbody></table>
		<table width="100%" height="20" border="0" cellspacing="0" cellpadding="0" id="tab_EPS">
			<tbody><tr>
				<td width="20%"><span>Amount:  </span></td>
			  	<td><input id="paymentAmount" value="" type="text" onchange="calPaymentMethodTotalForAdd()" class="textInput"></td>

			</tr>
		</tbody></table>
	</div>											
											

                                   	<div style="text-align:right;padding : 5px;margin : 5px;">
										<button class="btn-small btn-success">add</button>
                                   	</div>												
											
										</div>
										
										
                      				</div>
                      				<div>
                      				
                      				</div> <!-- row -->

									<div class="row" style="margin-top:10px">
									<div class="col-md-12">
			                            <!-- Info box -->
			                            <div class="box box-info">
			                                <div class="box-header">
			                                    <h3 class="box-title">Payment list</h3>
			                                </div>
			                                <div class="box-body">
											    <ul>
											    	<li>Cash ：2,000 HKD <small class="badge"><b>-</b></small></li>
											    	<li>Credit Card ：180 HKD <small class="badge"><b>-</b></small></li>
											    </ul>												
			                                </div><!-- /.box-body -->
			                                <div class="box-footer">
			                                    <code>Payment Total : 2,180 HKD</code>
			                                </div><!-- /.box-footer-->			                            
			                            </div><!-- /.box -->
			                        </div>
									</div>
                                   	<div style="text-align:center;padding : 5px;margin : 5px;">
										<button class="btn btn-success btn-lg block">Submit</button>
                                   	</div>										        
									

                    			</div>
                    		</div>
                    	</div>
                    </div>
                </div> <!-- row -->

                </section><!-- /.content -->

<script language="javascript">
var paymentMethodArr = [];

$(document).ready(function(){
	//initPaymentMethodData();
	choosePaymentMethod();
	$("#tab_paymentMethod input[type=radio]").click(function() { 
		choosePaymentMethod();
	});
});


function choosePaymentMethod(){
	var paymentMethod = $('input:radio[name=paymentMethod]:checked').val();
	if(!paymentMethod || paymentMethod == ""){
		paymentMethod = "200300";
		$('input:radio[name=paymentMethod][value=200300]').attr("checked","checked");
	}
	$("#tab_cash").hide();
	$("#tab_creditCardType").hide();
	$("#tab_cheque").hide();
	$("#tab_transfer").hide();
	$("#tab_EPS").hide();
	
	if(paymentMethod=="200300"){
		$("#tab_cash").show();
	}else if(paymentMethod=="200301"){
		$("#tab_creditCardType").show();
	}else if(paymentMethod=="200302"){
		$("#tab_cheque").show();
	}else if(paymentMethod=="200304"){
		$("#tab_EPS").show();
	}else if(paymentMethod=="200303"){
		$("#tab_transfer").show();
	}
}

</script>             