                <section class="content-header">
                    <h1>
                        Travel Agent
                        <small>Flights Book</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Flights Book</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <!-- START CUSTOM TABS -->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Custom Tabs -->
                            <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#tab_1" data-toggle="tab"><strong>One Way</strong></a></li>
                                    <li><a href="#tab_2" data-toggle="tab"><strong>Round Trip</strong></a></li>
                                    <li><a href="#tab_3" data-toggle="tab"><strong>Multi City</strong></a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_1">
                                    	<div style="background-color:#FFF7D4;">
	                                        <div class="form-group" style="display:inline-block;">
	                                            <label for="oneway_from">From</label>
	                                            <input type="text" class="form-control" id="oneway_from" placeholder="Shanghai-PVG">
	                                        </div>
	                                        <div class="form-group" style="display:inline-block;">
	                                            <label for="oneway_to">To</label>
	                                            <input type="text" class="form-control" id="oneway_to" placeholder="Hongkong-HKG">
	                                        </div>
											<!-- Date range -->
		                                    <div class="form-group" style="display:inline-block;">
		                                        <label>Depart Date: </label>
		                                        <input type="text" class="form-control" id="oneway_depart_date"/>
		                                    </div><!-- /.form group -->
	                                        
	                                        <div class="form-group" style="display:inline-block;">
	                                            <label>Adult(12+)</label>
												<select class="form-control">
													<option>0</option>
													<option>1</option>
													<option>2</option>
													<option>3</option>
													<option>4</option>
													<option>5</option>
												</select>	                                            
	                                        </div>
	                                        <div class="form-group" style="display:inline-block;">
	                                            <label>Child(2-12)</label>
												<select class="form-control">
													<option>0</option>
													<option>1</option>
													<option>2</option>
													<option>3</option>
													<option>4</option>
													<option>5</option>
												</select>	                                            
	                                        </div>
	                                        <div class="form-group" style="display:inline-block;">
	                                            <label>Infant(0-2)</label>
												<select class="form-control">
													<option>0</option>
													<option>1</option>
													<option>2</option>
													<option>3</option>
													<option>4</option>
													<option>5</option>
												</select>	                                            
	                                        </div>
                                    	</div>
                                    	<div style="background-color:#FFF7D4;">
	                                        <div class="form-group" style="display:inline-block;">
	                                            <label>Flights No.</label>
	                                            <input type="text" class="form-control" id="flight_no" placeholder="MU507">
	                                        </div>
	                                        <div class="form-group" style="display:inline-block;">
	                                            <label>Class</label>
												<select class="form-control">
													<option>Economy</option>
													<option>Business</option>
													<option>First class</option>
												</select>	                                            
	                                        </div>
                                    	</div>
                                    	<div style="text-align:center;padding : 5px;margin : 5px;">
											<button class="btn btn-success btn-lg block">Next</button>
                                    	</div>
					                    <div class="row">
					                        <!-- accepted payments column -->
					                        <!--  
					                        <div class="col-xs-12">
					                            <img class="imgZoomin" src="img/travel_demo/ordering.jpg" />
					                        </div>
					                        -->
					                        <div class="col-xs-7">
												<div>
									                <h4>Passenger 1 ：</h4>
									                
									                <input type="hidden" id="hidTravelerType" name="TravelerType" value="ADT">
									                
									                <!--姓名填写框-->
									                <table identify="select_name" class="passenger_field">
									                    <tbody><tr>
									                        <th>Name
									                        </th>
									                        <td>Passenger name must be identical with the name shown on the ID type selected.
									                        </td>
									                    </tr>
									                    <tr>
									                        <td colspan="2">
									                                <input type="text" placeholder="First name" class="ipt ipt_avator" name="firstname" valtype="required" maxlength="20">
									                                <input type="text" placeholder="Middle name (optional)" class="ipt" name="middlename" maxlength="20">
									                                <input type="text" placeholder="Last name" class="ipt" name="lastname" maxlength="20">
									                        </td>
									                    </tr>
									                </tbody></table>
									            </div>					
												<div style="margin:10px;BORDER-TOP: #bbbbbb 1px dashed; OVERFLOW: hidden; HEIGHT: 1px"></div>
												<div class="passenger_info" ptype="ADT">
									                <h4>Passenger 2 ：</h4>
									                
									                <input type="hidden" id="hidTravelerType" name="TravelerType" value="ADT">
									                
									                <!--姓名填写框-->
									                <table identify="select_name" class="passenger_field">
									                    <tbody><tr>
									                        <th>Name
									                        </th>
									                        <td>Passenger name must be identical with the name shown on the ID type selected.
									                        </td>
									                    </tr>
									                    <tr>
									                        <td colspan="2">
									                                <input type="text" placeholder="First name" class="ipt ipt_avator" name="firstname" valtype="required" maxlength="20">
									                                <input type="text" placeholder="Middle name (optional)" class="ipt" name="middlename" maxlength="20">
									                                <input type="text" placeholder="Last name" class="ipt" name="lastname" maxlength="20">
									                        </td>
									                    </tr>
									                </tbody></table>
									            </div>

												<div style="margin:10px;BORDER-TOP: #bbbbbb 1px dashed; OVERFLOW: hidden; HEIGHT: 1px"></div>
									            
												<div class="passenger_info" ptype="ADT">
									                <h4>Passenger 3：</h4>
									                
									                <input type="hidden" id="hidTravelerType" name="TravelerType" value="ADT">
									                
									                <!--姓名填写框-->
									                <table identify="select_name" class="passenger_field">
									                    <tbody><tr>
									                        <th>Name
									                        </th>
									                        <td>Passenger name must be identical with the name shown on the ID type selected.
									                        </td>
									                    </tr>
									                    <tr>
									                        <td colspan="2">
									                                <input type="text" placeholder="First name" class="ipt ipt_avator" name="firstname" valtype="required" maxlength="20">
									                                <input type="text" placeholder="Middle name (optional)" class="ipt" name="middlename" maxlength="20">
									                                <input type="text" placeholder="Last name" class="ipt" name="lastname" maxlength="20">
									                        </td>
									                    </tr>
									                </tbody></table>
									            </div>					
					                        </div><!-- /.col -->
					
					                        <div class="col-xs-5">
											<div class="your_summary">
											    <h3>Trip summary</h3>
										            
												<div class="flight_intro">     
												     <ul style="background-color:#FFF7D4;">
														 <li class="item01"><b>SHA-->HKG</b></li>
													 </ul>
												     <table class="step_intro">
												          <tbody>
												              <tr>
												                <td>
												                    Flight no. <b>MU507</b> (Economy)
												                </td>
												                <th>
												                	08-28-2014 08:55
												                </th>
												              </tr>
												         </tbody>
												    </table>

												</div>
												<div class="line"/>
										      	<div class="prices_detail">
										                <div class="detail_list">
										                    <h4>Fares</h4>
										                    <table style="width:100%">
										                        <tbody>                            
										                                <tr id="adtPriceInSummary" style="display: ;">
										                                    <th>
										                                        Adult
										                                    </th>
										                                    <td style="text-align:right">
																				1 x 
																				<input type="text" placeholder="unit price">
																				&nbsp;HKD
																			</td>
										                                </tr> 
										                                <tr id="chdPriceInSummary" style="display: ;">
										                                    <th id="chdPriceTitleInSummary">
										                                        Child
										                                    </th>
										                                    <td style="text-align:right">
																				1 x 
																				<input type="text" placeholder="unit price">
																				&nbsp;HKD
										                                   </td>
										                                </tr>  
										                        </tbody>
										                    </table>
										                </div>
										                <div class="detail_list">
										                    <h4>Taxes &amp; fees</h4>
										                    <table style="width:100%">
										                        <tbody>
										                                <tr id="aduTaxFeeInSummary" style="display: ;">
										                                    <th>
										                                        Adult
										                                    </th>
										                                    <td style="text-align:right">
																				1 x 
																				<input type="text" placeholder="unit price">
																				&nbsp;HKD
										                                    </td>
										                                </tr>                 
										                                <tr id="chdTaxFeeInSummary" style="display: ;">
										                                    <th >
										                                        Child
										                                    </th>
										                                    <td style="text-align:right">
																				1 x 
																				<input type="text" placeholder="unit price">
																				&nbsp;HKD
										                                    </td>
										                                </tr>
										                        </tbody>
										                    </table>
										                </div>
										
										            </div>
													<div class="line"/>
										            <dl class="total">
										                <dt><h4>Total</h4></dt>
										                <dd style="text-align:right">                
										                    
										                    HKD&nbsp;2,180
										                </dd>
										            </dl>
										        </div>					                            
					                            
					                            
					                        </div><!-- /.col -->
					
					                    </div><!-- /.row -->
                                    	<div style="text-align:center;padding : 5px;margin : 5px;">
											<button class="btn btn-success btn-lg block" onclick="toSettle();">Submit</button>
                                    	</div>
					                    

                                    </div>
                                    <div class="tab-pane" id="tab_2">
                                    	<div style="background-color:#FFF7D4;">
	                                        <div class="form-group" style="display:inline-block;">
	                                            <label for="oneway_from">From</label>
	                                            <input type="text" class="form-control" id="oneway_from" placeholder="Shanghai-PVG">
	                                        </div>
	                                        <div class="form-group" style="display:inline-block;">
	                                            <label for="oneway_to">To</label>
	                                            <input type="text" class="form-control" id="oneway_to" placeholder="Hongkong-HKG">
	                                        </div>
											<!-- Date range -->
		                                    <div class="form-group" style="display:inline-block;">
		                                        <label>Depart Date: </label>
		                                        <input type="text" class="form-control" id="roundtrip_depart_date"/>
		                                    </div><!-- /.form group -->
		                                    <div class="form-group" style="display:inline-block;">
		                                        <label>Return Date: </label>
		                                        <input type="text" class="form-control" id="roundtrip_return_date"/>
		                                    </div><!-- /.form group -->
	                                        
	                                        <div class="form-group" style="display:inline-block;">
	                                            <label>Adult(12+)</label>
												<select class="form-control">
													<option>0</option>
													<option>1</option>
													<option>2</option>
													<option>3</option>
													<option>4</option>
													<option>5</option>
												</select>	                                            
	                                        </div>
	                                        <div class="form-group" style="display:inline-block;">
	                                            <label>Child(2-12)</label>
												<select class="form-control">
													<option>0</option>
													<option>1</option>
													<option>2</option>
													<option>3</option>
													<option>4</option>
													<option>5</option>
												</select>	                                            
	                                        </div>
	                                        <div class="form-group" style="display:inline-block;">
	                                            <label>Infant(0-2)</label>
												<select class="form-control">
													<option>0</option>
													<option>1</option>
													<option>2</option>
													<option>3</option>
													<option>4</option>
													<option>5</option>
												</select>	                                            
	                                        </div>
                                    	</div>
                                    	<div style="background-color:#FFF7D4;">
	                                        <div class="form-group" style="display:inline-block;">
	                                            <label>Depart flights No.</label>
	                                            <input type="text" class="form-control" id="flight_no" placeholder="MU507">
	                                        </div>
	                                        <div class="form-group" style="display:inline-block;">
	                                            <label>Class</label>
												<select class="form-control">
													<option>Economy</option>
													<option>Business</option>
													<option>First class</option>
												</select>	                                            
	                                        </div>
	                                        <div class="form-group" style="display:inline-block;">
	                                            <label>Return flights No.</label>
	                                            <input type="text" class="form-control" id="flight_no" placeholder="MU507">
	                                        </div>
	                                        
	                                        <div class="form-group" style="display:inline-block;">
	                                            <label>Class</label>
												<select class="form-control">
													<option>Economy</option>
													<option>Business</option>
													<option>First class</option>
												</select>	                                            
	                                        </div>
                                    	</div>
                                 	
                                    	<div style="text-align:center;padding : 5px;margin : 5px;">
											<button class="btn btn-success btn-lg block">Next</button>
                                    	</div>
					                    <div class="row">
					                        <!-- accepted payments column -->
					                        <!--  
					                        <div class="col-xs-12">
					                            <img class="imgZoomin" src="img/travel_demo/ordering.jpg" />
					                        </div>
					                        -->
					                        <div class="col-xs-7">
												<div>
									                <h4>Passenger 1 ：</h4>
									                
									                <input type="hidden" id="hidTravelerType" name="TravelerType" value="ADT">
									                
									                <!--姓名填写框-->
									                <table identify="select_name" class="passenger_field">
									                    <tbody><tr>
									                        <th>Name
									                        </th>
									                        <td>Passenger name must be identical with the name shown on the ID type selected.
									                        </td>
									                    </tr>
									                    <tr>
									                        <td colspan="2">
									                                <input type="text" placeholder="First name" class="ipt ipt_avator" name="firstname" valtype="required" maxlength="20">
									                                <input type="text" placeholder="Middle name (optional)" class="ipt" name="middlename" maxlength="20">
									                                <input type="text" placeholder="Last name" class="ipt" name="lastname" maxlength="20">
									                        </td>
									                    </tr>
									                </tbody></table>
									            </div>					
												<div style="margin:10px;BORDER-TOP: #bbbbbb 1px dashed; OVERFLOW: hidden; HEIGHT: 1px"></div>
												<div class="passenger_info" ptype="ADT">
									                <h4>Passenger 2 ：</h4>
									                
									                <input type="hidden" id="hidTravelerType" name="TravelerType" value="ADT">
									                
									                <!--姓名填写框-->
									                <table identify="select_name" class="passenger_field">
									                    <tbody><tr>
									                        <th>Name
									                        </th>
									                        <td>Passenger name must be identical with the name shown on the ID type selected.
									                        </td>
									                    </tr>
									                    <tr>
									                        <td colspan="2">
									                                <input type="text" placeholder="First name" class="ipt ipt_avator" name="firstname" valtype="required" maxlength="20">
									                                <input type="text" placeholder="Middle name (optional)" class="ipt" name="middlename" maxlength="20">
									                                <input type="text" placeholder="Last name" class="ipt" name="lastname" maxlength="20">
									                        </td>
									                    </tr>
									                </tbody></table>
									            </div>

												<div style="margin:10px;BORDER-TOP: #bbbbbb 1px dashed; OVERFLOW: hidden; HEIGHT: 1px"></div>
									            
												<div class="passenger_info" ptype="ADT">
									                <h4>Passenger 3：</h4>
									                
									                <input type="hidden" id="hidTravelerType" name="TravelerType" value="ADT">
									                
									                <!--姓名填写框-->
									                <table identify="select_name" class="passenger_field">
									                    <tbody><tr>
									                        <th>Name
									                        </th>
									                        <td>Passenger name must be identical with the name shown on the ID type selected.
									                        </td>
									                    </tr>
									                    <tr>
									                        <td colspan="2">
									                                <input type="text" placeholder="First name" class="ipt ipt_avator" name="firstname" valtype="required" maxlength="20">
									                                <input type="text" placeholder="Middle name (optional)" class="ipt" name="middlename" maxlength="20">
									                                <input type="text" placeholder="Last name" class="ipt" name="lastname" maxlength="20">
									                        </td>
									                    </tr>
									                </tbody></table>
									            </div>					
					                        </div><!-- /.col -->
					
					                        <div class="col-xs-5">
											<div class="your_summary">
											    <h3>Trip summary</h3>
										            
												<div class="flight_intro">     
												     <ul style="background-color:#FFF7D4;">
														 <li class="item01"><b>SHA --> HKG</b> : Flight no. <b>MU507</b> (Economy) | 08-28-2014 08:55</li>
														 <li class="item01"><b>HKG --> SHA</b> : Flight no. <b>MU509</b> (Economy) | 09-28-2014 08:55</li>
													 </ul>
												</div>
												<div class="line"/>
										      	<div class="prices_detail">
										                <div class="detail_list">
										                    <h4>Fares</h4>
										                    <table style="width:100%">
										                        <tbody>                            
										                                <tr id="adtPriceInSummary" style="display: ;">
										                                    <th>
										                                        Adult
										                                    </th>
										                                    <td style="text-align:right">
																				1 x 
																				<input type="text" placeholder="unit price">
																				&nbsp;HKD
																			</td>
										                                </tr> 
										                                <tr id="chdPriceInSummary" style="display: ;">
										                                    <th id="chdPriceTitleInSummary">
										                                        Child
										                                    </th>
										                                    <td style="text-align:right">
																				1 x 
																				<input type="text" placeholder="unit price">
																				&nbsp;HKD
										                                   </td>
										                                </tr>  
										                        </tbody>
										                    </table>
										                </div>
										                <div class="detail_list">
										                    <h4>Taxes &amp; fees</h4>
										                    <table style="width:100%">
										                        <tbody>
										                                <tr id="aduTaxFeeInSummary" style="display: ;">
										                                    <th>
										                                        Adult
										                                    </th>
										                                    <td style="text-align:right">
																				1 x 
																				<input type="text" placeholder="unit price">
																				&nbsp;HKD
										                                    </td>
										                                </tr>                 
										                                <tr id="chdTaxFeeInSummary" style="display: ;">
										                                    <th >
										                                        Child
										                                    </th>
										                                    <td style="text-align:right">
																				1 x 
																				<input type="text" placeholder="unit price">
																				&nbsp;HKD
										                                    </td>
										                                </tr>
										                        </tbody>
										                    </table>
										                </div>
										
										            </div>
													<div class="line"/>
										            <dl class="total">
										                <dt><h4>Total</h4></dt>
										                <dd style="text-align:right">                
										                    
										                    HKD&nbsp;2,180
										                </dd>
										            </dl>
										        </div>					                            
					                            
					                            
					                        </div><!-- /.col -->
					
					                    </div><!-- /.row -->
                                    	<div style="text-align:center;padding : 5px;margin : 5px;">
											<button class="btn btn-success btn-lg block" onclick="toSettle();">Submit</button>
                                    	</div>
                                    </div>
                                    <div class="tab-pane" id="tab_3">


                                    	<div style="background-color:#FFF7D4;">
	                                        <div class="form-group" style="display:inline-block;">
	                                            <label for="oneway_from">From</label>
	                                            <input type="text" class="form-control" id="oneway_from" placeholder="Shanghai-PVG">
	                                        </div>
	                                        <div class="form-group" style="display:inline-block;">
	                                            <label for="oneway_to">To</label>
	                                            <input type="text" class="form-control" id="oneway_to" placeholder="Hongkong-HKG">
	                                        </div>
	                                        <div class="form-group" style="display:inline-block;">
	                                            <label>Flights No.</label>
	                                            <input type="text" class="form-control" id="flight_no" placeholder="MU507">
	                                        </div>	                                        
											<!-- Date range -->
		                                    <div class="form-group" style="display:inline-block;">
		                                        <label>Depart Date: </label>
		                                        <input type="text" class="form-control" id="oneway_depart_date"/>
		                                    </div><!-- /.form group -->
	                                        <div class="form-group" style="display:inline-block;">
	                                            <label>Class</label>
												<select class="form-control">
													<option>Economy</option>
													<option>Business</option>
													<option>First class</option>
												</select>	                                            
	                                        </div>
	                                        
	                                        <div class="form-group" style="display:inline-block;">
	                                            <label>Adult(12+)</label>
												<select class="form-control">
													<option>0</option>
													<option>1</option>
													<option>2</option>
													<option>3</option>
													<option>4</option>
													<option>5</option>
												</select>	                                            
	                                        </div>
	                                        <div class="form-group" style="display:inline-block;">
	                                            <label>Child(2-12)</label>
												<select class="form-control">
													<option>0</option>
													<option>1</option>
													<option>2</option>
													<option>3</option>
													<option>4</option>
													<option>5</option>
												</select>	                                            
	                                        </div>
	                                        <div class="form-group" style="display:inline-block;">
	                                            <label>Infant(0-2)</label>
												<select class="form-control">
													<option>0</option>
													<option>1</option>
													<option>2</option>
													<option>3</option>
													<option>4</option>
													<option>5</option>
												</select>	                                            
	                                        </div>
                                    	</div>


                                    	<div style="background-color:#FFF7D4;">
	                                        <div class="form-group" style="display:inline-block;">
	                                            <label for="oneway_from">From</label>
	                                            <input type="text" class="form-control" id="oneway_from" placeholder="Shanghai-PVG">
	                                        </div>
	                                        <div class="form-group" style="display:inline-block;">
	                                            <label for="oneway_to">To</label>
	                                            <input type="text" class="form-control" id="oneway_to" placeholder="Hongkong-HKG">
	                                        </div>
	                                        <div class="form-group" style="display:inline-block;">
	                                            <label>Flights No.</label>
	                                            <input type="text" class="form-control" id="flight_no" placeholder="MU507">
	                                        </div>	                                        
											<!-- Date range -->
		                                    <div class="form-group" style="display:inline-block;">
		                                        <label>Depart Date: </label>
		                                        <input type="text" class="form-control" id="oneway_depart_date"/>
		                                    </div><!-- /.form group -->
		                                    
	                                        <div class="form-group" style="display:inline-block;">
	                                            <label>Class</label>
												<select class="form-control">
													<option>Economy</option>
													<option>Business</option>
													<option>First class</option>
												</select>	                                            
	                                        </div>
	                                        
	                                        <div style="display:inline-block;text-align:left">
	                                        	<small class="badge"><b>+</b></small>
	                                        </div>		                                    
                                    	</div>

                                    	<div style="background-color:#FFF7D4;">
	                                        <div class="form-group" style="display:inline-block;">
	                                            <label for="oneway_from">From</label>
	                                            <input type="text" class="form-control" id="oneway_from" placeholder="Shanghai-PVG">
	                                        </div>
	                                        <div class="form-group" style="display:inline-block;">
	                                            <label for="oneway_to">To</label>
	                                            <input type="text" class="form-control" id="oneway_to" placeholder="Hongkong-HKG">
	                                        </div>
	                                        <div class="form-group" style="display:inline-block;">
	                                            <label>Flights No.</label>
	                                            <input type="text" class="form-control" id="flight_no" placeholder="MU507">
	                                        </div>	                                        
											<!-- Date range -->
		                                    <div class="form-group" style="display:inline-block;">
		                                        <label>Depart Date: </label>
		                                        <input type="text" class="form-control" id="oneway_depart_date"/>
		                                    </div><!-- /.form group -->
		                                    
	                                        <div class="form-group" style="display:inline-block;">
	                                            <label>Class</label>
												<select class="form-control">
													<option>Economy</option>
													<option>Business</option>
													<option>First class</option>
												</select>	                                            
	                                        </div>
	                                        
	                                        <div style="display:inline-block;text-align:left">
	                                        	<small class="badge"><b>+</b></small>
	                                        	<small class="badge"><b>-</b></small>
	                                        </div>		                                    
                                    	</div>
                                    	<div style="text-align:center;padding : 5px;margin : 5px;">
											<button class="btn btn-success btn-lg block">Next</button>
                                    	</div>


					                    <div class="row">
					                        <!-- accepted payments column -->
					                        <!--  
					                        <div class="col-xs-12">
					                            <img class="imgZoomin" src="img/travel_demo/ordering.jpg" />
					                        </div>
					                        -->
					                        <div class="col-xs-7">
												<div>
									                <h4>Passenger 1 ：</h4>
									                
									                <input type="hidden" id="hidTravelerType" name="TravelerType" value="ADT">
									                
									                <!--姓名填写框-->
									                <table identify="select_name" class="passenger_field">
									                    <tbody><tr>
									                        <th>Name
									                        </th>
									                        <td>Passenger name must be identical with the name shown on the ID type selected.
									                        </td>
									                    </tr>
									                    <tr>
									                        <td colspan="2">
									                                <input type="text" placeholder="First name" class="ipt ipt_avator" name="firstname" valtype="required" maxlength="20">
									                                <input type="text" placeholder="Middle name (optional)" class="ipt" name="middlename" maxlength="20">
									                                <input type="text" placeholder="Last name" class="ipt" name="lastname" maxlength="20">
									                        </td>
									                    </tr>
									                </tbody></table>
									            </div>					
												<div style="margin:10px;BORDER-TOP: #bbbbbb 1px dashed; OVERFLOW: hidden; HEIGHT: 1px"></div>
												<div class="passenger_info" ptype="ADT">
									                <h4>Passenger 2 ：</h4>
									                
									                <input type="hidden" id="hidTravelerType" name="TravelerType" value="ADT">
									                
									                <!--姓名填写框-->
									                <table identify="select_name" class="passenger_field">
									                    <tbody><tr>
									                        <th>Name
									                        </th>
									                        <td>Passenger name must be identical with the name shown on the ID type selected.
									                        </td>
									                    </tr>
									                    <tr>
									                        <td colspan="2">
									                                <input type="text" placeholder="First name" class="ipt ipt_avator" name="firstname" valtype="required" maxlength="20">
									                                <input type="text" placeholder="Middle name (optional)" class="ipt" name="middlename" maxlength="20">
									                                <input type="text" placeholder="Last name" class="ipt" name="lastname" maxlength="20">
									                        </td>
									                    </tr>
									                </tbody></table>
									            </div>

												<div style="margin:10px;BORDER-TOP: #bbbbbb 1px dashed; OVERFLOW: hidden; HEIGHT: 1px"></div>
									            
												<div class="passenger_info" ptype="ADT">
									                <h4>Passenger 3：</h4>
									                
									                <input type="hidden" id="hidTravelerType" name="TravelerType" value="ADT">
									                
									                <!--姓名填写框-->
									                <table identify="select_name" class="passenger_field">
									                    <tbody><tr>
									                        <th>Name
									                        </th>
									                        <td>Passenger name must be identical with the name shown on the ID type selected.
									                        </td>
									                    </tr>
									                    <tr>
									                        <td colspan="2">
									                                <input type="text" placeholder="First name" class="ipt ipt_avator" name="firstname" valtype="required" maxlength="20">
									                                <input type="text" placeholder="Middle name (optional)" class="ipt" name="middlename" maxlength="20">
									                                <input type="text" placeholder="Last name" class="ipt" name="lastname" maxlength="20">
									                        </td>
									                    </tr>
									                </tbody></table>
									            </div>					
					                        </div><!-- /.col -->
					
					                        <div class="col-xs-5">
											<div class="your_summary">
											    <h3>Trip summary</h3>
										            
												<div class="flight_intro">     
												     <ul style="background-color:#FFF7D4;">
														 <li class="item01"><b>SHA --> HKG</b> : Flight no. <b>MU507</b> (Economy) | 08-28-2014 08:55</li>
														 <li class="item01"><b>HKG --> SHA</b> : Flight no. <b>MU509</b> (Economy) | 09-28-2014 08:55</li>
													 </ul>
												</div>
												<div class="line"/>
										      	<div class="prices_detail">
										                <div class="detail_list">
										                    <h4>Fares</h4>
										                    <table style="width:100%">
										                        <tbody>                            
										                                <tr id="adtPriceInSummary" style="display: ;">
										                                    <th>
										                                        Adult
										                                    </th>
										                                    <td style="text-align:right">
																				1 x 
																				<input type="text" placeholder="unit price">
																				&nbsp;HKD
																			</td>
										                                </tr> 
										                                <tr id="chdPriceInSummary" style="display: ;">
										                                    <th id="chdPriceTitleInSummary">
										                                        Child
										                                    </th>
										                                    <td style="text-align:right">
																				1 x 
																				<input type="text" placeholder="unit price">
																				&nbsp;HKD
										                                   </td>
										                                </tr>  
										                        </tbody>
										                    </table>
										                </div>
										                <div class="detail_list">
										                    <h4>Taxes &amp; fees</h4>
										                    <table style="width:100%">
										                        <tbody>
										                                <tr id="aduTaxFeeInSummary" style="display: ;">
										                                    <th>
										                                        Adult
										                                    </th>
										                                    <td style="text-align:right">
																				1 x 
																				<input type="text" placeholder="unit price">
																				&nbsp;HKD
										                                    </td>
										                                </tr>                 
										                                <tr id="chdTaxFeeInSummary" style="display: ;">
										                                    <th >
										                                        Child
										                                    </th>
										                                    <td style="text-align:right">
																				1 x 
																				<input type="text" placeholder="unit price">
																				&nbsp;HKD
										                                    </td>
										                                </tr>
										                        </tbody>
										                    </table>
										                </div>
										
										            </div>
													<div class="line"/>
										            <dl class="total">
										                <dt><h4>Total</h4></dt>
										                <dd style="text-align:right">                
										                    
										                    HKD&nbsp;2,180
										                </dd>
										            </dl>
										        </div>					                            
					                            
					                            
					                        </div><!-- /.col -->
					
					                    </div><!-- /.row -->
                                    	<div style="text-align:center;padding : 5px;margin : 5px;">
											<button class="btn btn-success btn-lg block" onclick="toSettle();">Submit</button>
                                    	</div>


                                    </div><!-- /.tab-pane -->
                                </div><!-- /.tab-content -->
                            </div><!-- nav-tabs-custom -->
                        </div><!-- /.col -->


                    </div> <!-- /.row -->
                    <!-- END CUSTOM TABS -->

                </section><!-- /.content -->

<script type="text/javascript">

	//jQueryUI datapicker
    $("#oneway_depart_date").datepicker({
        defaultDate: "null",
        changeMonth: true,
        numberOfMonths: 2
    });


    $("#roundtrip_depart_date").datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 1,
        onClose: function(selectedDate) {
            $("#roundtrip_return_date").datepicker("option", "minDate", selectedDate);
        }
    });
    $("#roundtrip_return_date").datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 1,
        onClose: function(selectedDate) {
            $("#roundtrip_depart_date").datepicker("option", "maxDate", selectedDate);
        }
    });
    
    
    function toSettle(){
    	LoadAjaxContent("pages/examples/settlement.jsp");
    }
    
</script>

<style>
<!--

-->
</style>
